package com.mjw.spring.cloud.client.controller;

import com.mjw.spring.cloud.server.api.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Note：
 *
 * @author ：mengjw
 * @description：客户端提供的用户服务
 * @date ：Created in 2020/9/10
 */
@Slf4j
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("getUserName")
    public String getUserName() {
        return userService.getUserName("mjw");
    }
}