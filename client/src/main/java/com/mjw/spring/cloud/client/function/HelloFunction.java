package com.mjw.spring.cloud.client.function;

import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Note：
 *
 * @author ：mengjw
 * @description：自定义函数接口
 * @date ：Created in 2020/9/10
 */
@Component("hello")
public class HelloFunction implements Function<String, String> {

    @Override
    public String apply(String s) {
        return "Hello：" + s;
    }
}