package com.mjw.spring.cloud.client.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Note：
 *
 * @author ：mengjw
 * @description：全局的负载均衡策略
 * @date ：Created in 2020/9/10
 */
@Configuration
public class RibbonGlobalLoadBalancingConfiguration {

    /**
     * 轮询
     */
    @Bean
    public IRule ribbonRule() {
        return new RoundRobinRule();
    }

}