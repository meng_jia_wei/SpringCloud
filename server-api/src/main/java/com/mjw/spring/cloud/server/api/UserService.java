package com.mjw.spring.cloud.server.api;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Note：
 *
 * @author ：mengjw
 * @description：rpc调用服务端的客户服务的接口
 * @date ：Created in 2020/9/10
 */
@FeignClient(value = "openfeign-server")
@RequestMapping(path = "/user")
public interface UserService {

    /**
     * 获取用户名
     *
     * @param userId
     * @return
     */
    @GetMapping("getUserName")
    String getUserName(@RequestParam("userId") String userId);

    /**
     * 获取用户年龄
     *
     * @param userId
     * @return
     */
    @GetMapping("getUserAge")
    String getUserAge(@RequestParam("userId") String userId);

}