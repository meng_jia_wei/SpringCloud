package com.mjw.spring.cloud.server.cache;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

/**
 * Note：
 *
 * @author ：mengjw
 * @description：用户缓存
 * @date ：Created in 2020/10/27
 */
@Component
public class UserCache {

    private static final String MJW = "mjw";

    @Cacheable(value = "userName", key = "#userId")
    public String getUserName(String userId) {
        if (MJW.equals(userId)) {
            return "孟佳伟";
        } else {
            return "佚名";
        }
    }

    @Cacheable(value = "userAge", key = "#userId")
    public String getUserAge(String userId) {
        if (MJW.equals(userId)) {
            return "18";
        } else {
            return "24";
        }
    }
}