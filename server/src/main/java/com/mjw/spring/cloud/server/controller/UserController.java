package com.mjw.spring.cloud.server.controller;

import com.mjw.spring.cloud.server.cache.UserCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Note：服务端提供的http协议的用户服务
 *
 * @author ：mengjw
 * @description：服务端提供的用户服务
 * @date ：Created in 2020/9/10
 */
@RestController
@RequestMapping(path = "/user")
public class UserController {

    @Autowired
    private UserCache userCache;

    /**
     * 获取用户名
     *
     * @param userId
     * @return userName
     */
    @GetMapping("getUserName")
    public String getUserName(@RequestParam("userId") String userId) {
        return userCache.getUserName(userId);
    }

    /**
     * 获取用户年龄
     *
     * @param userId
     */
    @GetMapping("getUserAge")
    public String getUserAge(@RequestParam("userId") String userId) {
        return userCache.getUserAge(userId);
    }

}