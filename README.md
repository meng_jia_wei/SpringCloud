# SpringCloud的简单工程

## server-api
服务端提供的api接口

## config
配置中心

## server
服务端通过openfeign将接口暴露服务到eureka

## client
客户端发现服务端的服务并完成调用

## gateway
统一接入的网关